from django.urls import path
from . import views

app_name = 'airport'

urlpatterns = [
    path('', views.add_airport, name='add_airport'),
    path('<str:code>', views.get_airport, name='get_airport'),
    path('delete/<str:code>', views.delete_airport, name='delete_airport'),
    path('update/<str:code>', views.update_airport, name='update_airport'),
    path('photos/<str:code>', views.add_airport_photo, name='add_airport_photo'),
    path('photos/get/<str:code>', views.get_airport_photo, name='get_airport_photo'),
]
