from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Airport

@api_view(['POST'])
def add_airport(request):
    code = request.POST['code']
    name = request.POST['name']
    city = request.POST['city']
    state = request.POST['state']

    try:
        existing_airport = Airport.objects.get(code=code)
        return Response({
            "status":"400",
            "message":"Data airport tersebut sudah ada, silakan tambahkan yang lain."
        }, status = status.HTTP_400_BAD_REQUEST)
    except:
        new_airport = Airport(code=code, name=name, city=city, state=state)
        new_airport.save()
        return Response({
            "status":"200",
            "code":new_airport.code,
            "name":new_airport.name,
            "city":new_airport.city,
            "state":new_airport.state,
            "photo":new_airport.photo.name
        })

@api_view(['GET'])
def get_airport(request, code):
    try:
        existing_airport = Airport.objects.get(code=code)
        return Response({
            "status":"200",
            "code":existing_airport.code,
            "name":existing_airport.name,
            "city":existing_airport.city,
            "state":existing_airport.state,
            "photo":existing_airport.photo.name
        })
    except:
        return Response({
            "status":"400",
            "message":"Data airport tidak tersedia untuk code tersebut."
        }, status = status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def delete_airport(request, code):
    try:
        existing_airport = Airport.objects.get(code=code)
        existing_airport.delete()
        return Response({
            "status":"200",
            "message":"Data airport telah berhasil dihapus."
        })
    except:
        return Response({
            "status":"400",
            "message":"Data airport tidak tersedia untuk code tersebut."
        }, status = status.HTTP_400_BAD_REQUEST)

@api_view(['PUT'])
def update_airport(request, code):
    name = request.POST['name']
    city = request.POST['city']
    state = request.POST['state']
    try:
        existing_airport = Airport.objects.get(code=code)
        existing_airport.name = name
        existing_airport.city = city
        existing_airport.state = state
        existing_airport.save()

        return Response({
            "status":"200",
            "code":existing_airport.code,
            "name":existing_airport.name,
            "city":existing_airport.city,
            "state":existing_airport.state,
            "photo":existing_airport.photo.name
        })
    except:
        return Response({
            "status":"400",
            "message":"Data airport tidak tersedia untuk code tersebut."
        }, status = status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def add_airport_photo(request, code):
    photo = request.FILES['photo']
    try:
        existing_airport = Airport.objects.get(code=code)
        existing_airport.photo = photo
        existing_airport.save()

        return Response({
            "status":"200",
            "code":existing_airport.code,
            "name":existing_airport.name,
            "city":existing_airport.city,
            "state":existing_airport.state,
            "photo":existing_airport.photo.name
        })
    except:
        return Response({
            "status":"400",
            "message":"Data airport tidak tersedia untuk code tersebut."
        }, status = status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_airport_photo(request, code):
    try:
        existing_airport = Airport.objects.get(code=code)
        airport_photo = existing_airport.photo.name

        with open(airport_photo, 'rb') as file:
            return HttpResponse(file.read(), content_type="image/png")
    except:
        return Response({
            "status":"400",
            "message":"Data airport tidak tersedia untuk code tersebut."
        }, status = status.HTTP_400_BAD_REQUEST)