from django.db import models

# Create your models here.
class Airport(models.Model):
    code=models.CharField(primary_key=True, max_length=3)
    name=models.CharField(max_length=20)
    city=models.CharField(max_length=20)
    state=models.CharField(max_length=20)
    photo = models.FileField(null=True, upload_to='airport/photos')